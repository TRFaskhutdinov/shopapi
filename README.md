# ShopApi

## Отказ от заказа

UserRole : customer

Post

https://localhost:44374/api/order/denyorder

#### body:

| key | value's type|
| --- | --- |
| UserId | int |
| RoleName | string |
| OrderId | int |

## Жалоба на заказ

UserRole : customer

Post

https://localhost:44374/api/order/complaint

#### body:

| key | value's type|
| --- | --- |
| UserId | int |
| RoleName | string |
| ProductId | int |
| Text | string (max 5000)|

## Изменения статуса самовывоза

UserRole : customer

Post

https://localhost:44374/api/order/delivery

#### body:

| key | value's type|
| --- | --- |
| UserId | int |
| RoleName | string |
| OrderId | int |
| IsSelfDelivery | bool |

## Отмена завершения доставки

UserRole : courier

Post

https://localhost:44374/api/order/denydeliveryfulfilment

#### body:

| key | value's type|
| --- | --- |
| UserId | int |
| RoleName | string |
| OrderId | int |
